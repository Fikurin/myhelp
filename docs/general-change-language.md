---
title: Change System Language
---
**Change language of MyMeet**

## **How do I change language of MyMeet?**

To change **Language** of MyMeet:

1. Click <img src="/images/General - Change language/media/image1.png" width="37" height="37" /> at the top left of any MyMeet Page.

Choose and click **Switch to** or **Bytte til**. **NO** for Norwegian and **ENG** for English.
