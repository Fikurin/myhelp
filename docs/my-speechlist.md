---
title: Speaker/Speech List - My Speechlist
---


## **How do i know when all my speech time** **?**

1. Go to **MyMeet**. Login to your MyMeet account
2. On the sidebar click My Speechlist menu

> <img src="/images/my speechlist/media/image1.png" width="197" height="72" />

1. My Speechlist will appear in list order by date created. <img src="/images/my speechlist/media/image2.png" width="601" height="195" />
2. Speechlist only appear when you have a speech time on event you participated, be ready for your speech time and have fun
