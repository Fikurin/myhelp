---
title: "Participants "
---

::: tip Warning!!
This page can be accessed only by admin.
:::

**Managing Events - Participant**

You can add Participant for your MyMeet Events, there are many ways to add participants to the MyMeet event, the first way is to add it manually by adding some of the participant's information, or by importing participant data in CSV or Excel format.Top of Form

## **How can I add Participant for events in MyMeet?**

To see your **Participants** page of MyMeet account:

- Click <img src="/images/Event - Participant/media/image1.png" width="106" height="" /> at the top left under **Dashboard** icon of any MyMeet Page. You’ll be on the **Managing Events** page of MyMeet.
- Select the **Event** for which you will add participant in **Event List**.

> <img src="/images/Event - Participant/media/image3.png" width="365" height="182" />

- Then click <img src="/images/Event - Participant/media/image2.png" width="166" height="20" /> button. You’ll be on the **Participants** page of MyMeet.
- Add Participants in the **Managing participant** section, then look for
  the **red button** like this
  <img src="/images/Event - Participant/media/image4.png" width="35" height="34" /> .
- There are two options for add participants, with manually or by importing them (CSV / Excel). You have 2 options like this:
- This is the first option, **Add participant** to events :
- Click **Add Participant** button <img src="/images/Event - Participant/media/image5.png" width="125" height="" />
- Enter participant information details on the following form:
  As you can see there’s several form to fill, such as:
- **First Name** : please fill with user name.
- **Last Name** : please fill with user last name.
- **Email** : please fill with user email.
- **Password** : please fill with user password.
- **Confirmation Password** : please fill with user confirmation password.
- And then you can set Delegate ID and type of participant for each participant on the following form:
- The types that can be set in each participant are as follows:

 <img src="/images/Event - Participant/media/image9.png" width="137" height="104" />

- Determine the rights given to participants by checking the box, available rights to Submit Proposals, Speak, and Voting Rights for each participant:

> <img src="/images/Event - Participant/media/image9.png" width="137" height="104" />

- Enter Union/Company information details on the following form:

> <img src="/images/Event - Participant/media/image10.png" width="327" height="172" />

As you can see there’s several form to fill, such as:

- **Union** : please fill with user union.
- **Branch** : please fill with user branch.
- **Phone** : please fill with user phone.
- **Job Title** : please fill with user job title.
- **Gender** : please fill with user Gender.
- You can set participants to receive various notifications in MyMeet events, by checking or not in the box as below:

 <img src="/images/Event - Participant/media/image11.png" width="333" height="119" />

- And then click the save button for saving your participant data, or Back button for going back, the button as below:

 <img src="/images/Event - Participant/media/image12.png" width="140" height="41" />

- This is the second option, **Import File** participant to events :

- Click **Import File** button <img src="/images/Event - Participant/media/image5.png" width="125" height="" />
- You can drag **File** or click to search the file on your computer directory for upload your file, you can see as below :
- Then click the **Upload** button.

**How can I view the detail information for a specific participant?**

- Click on three dots on a list of participant you want to see. You will see 4 options there, click **View Participant.**
- As you can see, there is detailed participant information that you can see, including barcodes that each participant will have.

**How can I edit rights for a specific participant?**

- Click on three dots on a list of participant you want to edit rights. You will see 4 options there, click **Edit Rights.**
- You can check which rights box you want to give, then click save.

**How can I edit detail information for a specific participant?**

- Click on three dots on a list of participant you want to delete. You will see 4 options there, click **Edit.**
- You can **Edit Participant** details**,** starting from the name to the notification.<img src="/images/Event - Participant/media/image17.png" width="566" height="268" />

**How can I delete participant?**

- Click on three dots on a list of participant you want to delete. You will see 4 options there, click **Delete.**
- You can click **Delete** to remove participant or **Cancel** to stop delete process.<img src="/images/Event - Participant/media/image18.png" width="561" height="202" />

**How can I export my participant list? **

- For to export participant data to an excel / CSV file, you can click the **export participant** button as below:

 <img src="/images/Event - Participant/media/image19.png" width="500" height="" />

**How can I remove all my participant list? **

- To remove all participant data, you can click the **remove all participant** button as below:
  <img src="/images/Event - Participant/media/image19.png" width="500" height="" />

**Search participant list with a search box.**

- To searching a specific participant data, you can use the **Search** box as below:
  <img src="/images/Event - Participant/media/image19.png" width="500" height="" />

**Filtering participant list for a certain amount.**

- To show participant data for a specific range, you can fill the **Show** box with a specific number as below:
  <img src="/images/Event - Participant/media/image19.png" width="500" height="" />
