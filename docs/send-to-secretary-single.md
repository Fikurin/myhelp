---
title: Suggestions - Send Single Suggestion to Secretary
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## **How do I send single suggestion to Secretary?**

- <span id="_Hlk530904872" class="anchor"><span id="_Hlk530905758" class="anchor"></span></span>Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click **Now** menu

> <img src="/images/now  - send to secretary single/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic
- click on the topic you need to participate
- send suggestion to secretary one by one click on the three dots <img src="/images/now  - send to secretary single/media/image3.png" width="22" height="35" /> and then new button will appear <img src="/images/now  - send to secretary single/media/image4.png" width="487" height="161" />
- click sent to secretary to send this suggestions to secretary
