---
title: Messages 
---

::: tip Warning!!
This page can be accessed only by all role except guest.
:::


## **How do I Send messages to other participant?**

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click Message menu
- To add new message you can click on the icon <img src="/images/messages/media/image2.png" alt="" width="40" height="40" /> on the right corner and form to send message will pop out
- As you can see there’s several form to fill, such as :
  - **Recipient** : please fill with your target recipient.
  - **Message Type** : please fill with your message type.
  - There’s several type for message

> <img src="/images/messages/media/image4.png" alt="Sebuah gambar berisi cuplikan layar, luar ruangan Deskripsi dihasilkan secara otomatis" width="169" height="139" />

- **General :** type for general message you want to send to other participant
- **Inquiry :** type for inqury message to secretary or other participant
- **Leave :** type for leave excuce or sick leave, this type will have diffrent form and add new form such; **Start Time** and **End Time**
- **Secretary** will review your sick leave message and deactive your account for event you participated as your request accepted
- **Broadcast :** type of message that can send to all participant

<!-- -->

- **Title** : please fill with your user email.
- **Message** : please fill with your user password, you can give default password for your user. They can change it later.
- **Submit** : button you can click to send message
- **Draft :** button you can click to save your message as draft
- **Cancel :** button you can click to cancel message process

## **How do i reply message ?**

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click Message menu
- To **Read** message you can click on the list of the message and read the message
  <img src="/images/messages/media/image6.png" alt="Sebuah gambar berisi cuplikan layar, monitor, dalam ruangan Deskripsi dihasilkan secara otomatis" width="601" height="117" />
- Message details will appear
- Click on the **Answer** button to reply and new message as reply form will pop out
  <img src="/images/messages/media/image8.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="300" height="236" />
- As you can see the Recipient will automatically as the message sender
- As you can see there’s several form to fill, such as :
  - **Recipient** : please fill with your target recipient.
  - **Message Type** : please fill with your message type.
  - There’s several type for message

> <img src="/images/messages/media/image4.png" alt="Sebuah gambar berisi cuplikan layar, luar ruangan Deskripsi dihasilkan secara otomatis" width="169" height="139" />

- **General :** type for general message you want to send to other participant
- **Inquiry :** type for inqury message to secretary or other participant
- **Leave :** type for leave excuce or sick leave, this type will have diffrent form and add new form such; **Start Time** and **End Time**
- **Secretary** will review your sick leave message and deactive your account for event you participated as your request accepted
- **Broadcast :** type of message that can send to all participant

<!-- -->

- **Title** : please fill with your user email.
- **Message** : please fill with your user password, you can give default password for your user. They can change it later.
- **Submit** : button you can click to send message
- **Draft :** button you can click to save your message as draft
- **Cancel :** button you can click to cancel message process

> Tip sorting your messages
>
> - You can use several filter on your message inbox
> - On your right top corner you will see some red button with three dot <img src="/images/messages/media/image9.png" alt="Sebuah gambar berisi objek Deskripsi dihasilkan secara otomatis" width="50" height="30" />
> - When you click that button you will see some filter button appear

**Default options is View All which view all message without any filter**

- **View All** : show all messages without any filter
- **View Unread** : show only unread message
- **View Draft** : show your saved message draft
- **View Sent Item** : show your current sent messages
- **View General** : show only general type of message
- **View inquiry** : show only inquiry type of message
- **View leave** : show only Leave type of message

## How to do quick reply messages to approve or not approve ?

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click Message menu
- To **Read** message you can click on the list of the message and read the message
- click on the message on your inbox list

> <img src="/images/messages/media/image11.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="200" height="236" />

- As you can see form of secretary message
- You can click on the Quick Answer to approve or dissapprove

**1. How to I forward message?**

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click **Message** menu
- To **Read** message you can click on the list of the message and read the message
- click on the message on your inbox list

> <img src="/images/messages/media/image13.png" width="200" height="197" />

- As you can see form of secretary message
- You can click on the forward to send message to admin

**2. How I display participant sick leave?**

- Go to **MyMeet. **Login to your MyMeet account
- On the sidebar click **Message** menu

  <img src="/images/messages/media/image1.png" width="262" height="113" />

- To **Read** message you can click on the list of the message and read the message

  <img src="/images/messages/media/image6.png" width="601" height="117" />

- click on the message on your inbox list

> <img src="/images/messages/media/image13.png" width="200" height="179" />

- As you can see form of secretary message
- You can click on the **display** to display message to audience
