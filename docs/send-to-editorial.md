---
title: Send to editorial
---

::: tip Warning!!
This page can be accessed only by admin.
:::


## **How do I send suggestions to editorial?**

 <span id="_Hlk530904872" class="anchor"><span id="_Hlk530905758" class="anchor"></span></span>

* Go to MyMeet Login to your MyMeet account
* On the sidebar click **Now** menu

> <img src="/images/now - send to editorial abc/media/image1.png" width="137" height="309" />

* then go to the active topic and click the topic

  <img src="/images/now - send to editorial abc/media/image2.png" width="601" height="150" />

* click on the topic you need to participate
* send suggestion to secretary one by one click on the three dots <img src="/images/now - send to editorial abc/media/image3.png" width="22" height="35" /> and then new button will appear <img src="/images/now - send to editorial abc/media/image4.png" width="487" height="161" />
* click sent to editorial to send this suggestions to your desired editorial. Name of editorial will appear as you name it, you can pick one off your desired editorial team
