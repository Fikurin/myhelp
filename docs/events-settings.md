---
title: "Settings (Display, Pause) "
---

::: tip Warning!!
This page can be accessed only by admin.
:::

**Managing Events – Settings**

<span id="_Hlk530423468" class="anchor"></span>You can **Settings Pause session display, Custom Vote display, Display General,** or **Display Speech list** on the **Setting** page of MyMeet.

## **How can I create a display for Pause Session of events in MyMeet?**

To see your **Settings** page of MyMeet:

1. Click <img src="/images/Events - Settings/media/image1.png" width="106" height="" /> at the top left under **Dashboard** icon of any MyMeet Page. You’ll be on the **Managing Events** page of MyMeet.
2. Select the **Event** for which you will add Agenda in **Event List**.

> <img src="/images/Events - Settings/media/image3.png" width="365" height="182" />

3. Then click the <img src="/images/Events - Settings/media/image2.png" width="163" height="20" /> button. You’ll be on the **Settings** page of MyMeet.
4. There are 4 tab sections in the **Settings**, the first is **Pause section**, as below:
   As you can see there’s several tabs, such as:

- **Pause session** :
  - You can Add Graphics for pause display in the **Setting** on the event you selected, just click for the <img src="/images/Events - Settings/media/image5.png" width="25" height="27" /> button. This setting will be useful for display during the pause event. Popup will appear, fill out the Name, File form, then click **Submit** button if done or **Cancel** button for cancelling the action
  - You can set the timer for a pause on a specific event, as below:
    <img src="/images/Events - Settings/media/image7.png" width="300" height="" />
  - Then you can see the preview of pause display event on the right side, as below:
    <img src="/images/Events - Settings/media/image8.png" width="300" height="" />
  - Then you can see all the list of pause display on the left side, as below:
  - For start or stop the **pause display** you can click **Start** blue button <img src="/images/Events - Settings/media/image9.png" width="300" height="" />
- Or you can start **pause screen** with click **three dots** button on left, there 2 options
  <img src="/images/Events - Settings/media/image10.png" width="86" height="" /> choose for start **pause screen**.
- If you start **pause display,** when you go to **Now** page, the page will show the **Screen** of pause that you have made before.
- If you want to stop **pause display,** you can click **stop **button.
- If you stop **pause display,** when you go to **Now** page, the page will be normal as it should be.
- For delete **Pause display,** click the **three dots** button <img src="/images/Events - Settings/media/image10.png" width="86" height="" />, there 2 options, choose to <img src="/images/Events - Settings/media/image12.png" width="100" height="" />remove a specific **Pause display** on a list.

<!-- -->

## **Display General** 
  - How can I create a **Display General?** Click “+” button.
  - Popup will appear, fill out the Name, File form, then click **Submit** button if done or **Cancel** button for cancelling the action
  - You can see a preview display where the **Display** it’s displayed.
    <img src="/images/Events - Settings/media/image27.png" width="380" height="" />
  - You can set up the formatted **Display** for general, start from font to alignment.
    <img src="/images/Events - Settings/media/image28.png" width="380" height="" />
  - Click **Save** button if done or **Cancel** button for cancelling the action.
## **Display Speechlist**
  - How can I set up **Display** for **Speechlist?** You can set Display image for Speechlist, click **Upload File** Button.
  - Popup will appear, fill out the Name, File form, then click **Submit** button if done or **Cancel** button for cancelling the action.
  - If want to set background graphics from your library, you can click **Select from library** button.
    <img src="/images/Events - Settings/media/image30.png" width="380" height="" />
  - Popup will appear, choose the image you want to make the background graphics, then click **Done** button if done.
## **How can I set-up Content for display speechlist?**
  - Go to the right side on the **Setting** page. Set the heading, there are 2 options for an **Active topic** or **none.** You can **Custom text** with filling out the form of **Custom text.**
  - Set padding top, padding bottom, padding left and padding right.
    <img src="/images/Events - Settings/media/image33.png" width="380" height="" />
  - Set-up for **Active Speaker.** On **First Row,** you can set-up **First Field** that you can show delegate number or none, **Second Field** you can show full name or none, and **Second Row** you can set-up for **First Field** showing for the union, job title or none.
    <img src="/images/Events - Settings/media/image34.png" width="380" height="" />
  - Set-up for **Next Speaker.** On **First Row** you can set-up **Number of the list, First Field** that you can show delegate number or none, **Second Field** you can show full name or none.<img src="/images/Events - Settings/media/image35.png" width="380" height="64" />
  - Set-up for Advance, that you can set **font and alignment** setting with CSS code. Click **Save** button if you done set-up the **Content,** or **Cancel** button for cancelling the action.
    <img src="/images/Events - Settings/media/image36.png" width="380" height="" />
