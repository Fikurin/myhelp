---
title: "Permissions"
---

::: tip Warning!!
This page can be accessed only by admin.
:::

<span id="_Hlk530118908" class="anchor"></span>

## **How do I deactive Event participan t ?**

- Go to **MyMeet.** Login to your MyMeet account as an admin of the event

* Go to **EVENTS** menu on the sidebar

- # Now you select event you want to edit

   <img src="/images/event permission (admin)/media/image1.png" width="300" height="" />

- # And select permissions menu on the right side

   <img src="/images/event permission (admin)/media/image2.png" width="601" height="304" />

- And then select users you want to deactive or active, you will see two green dot this indicate participant is all active, the first dot is indicate deactived by admin and the second dot is indicate participant deactive by sick leave or by other request.
  <img src="/images/event permission (admin)/media/image3.png" width="601" height="236" />
