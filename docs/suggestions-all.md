---
title: Suggestions - All Suggestions
---


## **How do I see all suggestions?**

- Go to **MyMeet**. Login to your MyMeet account
- On the sidebar click Suggestions menu

 <img src="/images/suggestions all/media/image1.png" width="197" height="113" />

- All suggestions will appear in list order by date created. <img src="/images/suggestions all/media/image2.png" width="601" height="238" />
- You can see every suggestion details after you click <img src="/images/suggestions all/media/image3.png" width="22" height="35" /> and Details button will appear

 <img src="/images/suggestions all/media/image4.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="180" height="89" />

- Click Details and you will see the suggestions details

 <img src="/images/suggestions all/media/image5.png" width="484" height="236" />

- Click back to go <img src="/images/suggestions all/media/image6.png" width="81" height="35" />to the previous page
