module.exports = {
    title: "MyHelp",
    description: "Your friendly documentations for MyMeet",
    themeConfig: {
        nav: [{
            text: "Getting Started",
            link: "/login-logout"
        }],

        sidebar: [{
                title: "General",
                children: [
                    "/login-logout",
                    "/general-change-language",
                    "/general-forgot-password",
                    "/general-profile"
                ],
                collapsable: true
            },
            // {
            //     title: "Manage Clients",
            //     collapsable: true,
            //     children: ["/admin-client"]
            // },
            // {
            //     title: "Manage Users",
            //     children: ["/user"],
            //     collapsable: true
            // },
            {
                title: "Manage Events",
                children: ["/client-event"],
                collapsable: true
            },
            {
                title: "Event Features",
                children: [
                    "/agenda",
                    "/export-meeting",
                    "/events-invitations",
                    "/events-participant",
                    "/events-permission",
                    "/events-reporting",
                    "/events-settings",
                    "/editorial",
                    "/messages",
                    "/send-editorial",
                    "/suggestions-details",
                    "/suggestions-all"
                ],
                collapsable: true
            },
            {
                title: "Event Now",
                children: [
                    "/change-order",
                    "/activate-the-topic",
                    "/change-time",
                    "/close-topic",
                    "/create-speech-list",
                    "/edit-topic",
                    "/editorial",
                    "/last-speaker",
                    "/my-speechlist",
                    "/my-suggestions",
                    "/new-suggestion",
                    "/remove-speechlist",
                    "/share-delegates",
                    "/stop-speech",
                    "/stop-suggestions",
                    "/stop-topic"
                ],
                collapsable: true
            }
        ]

        // sidebar: [{
        //         title: 'Welcome',
        //         collapsable: true,
        //         children: [
        //             '/welcome'
        //         ]
        //     },
        //     {
        //         title: 'Getting Started Guide',
        //         collapsable: false,
        //         children: [
        //         '/Agenda',
        //         '/api',
        //         '/change-order',
        //         '/change-time',
        //         '/client-event',
        //         '/close-topic',
        //         '/create-speech-list',
        //         '/edit-topic',
        //         '/Editorial-Teams',
        //         '/Editorial',
        //         '/Events-Invitations',
        //         '/Events-Participant',
        //         '/Events-permission',
        //         '/Events-Reporting',
        //         '/Events-Settings',
        //         '/export-meeting',
        //         '/General-Change-language',
        //         '/General-forgot-password',
        //         '/General-Profile',
        //         '/guide',
        //         '/last-speaker',
        //         '/Login-Logout',
        //         '/messages',
        //         '/my-speechlist',
        //         '/my-suggestions',
        //         '/new-suggestion',
        //         '/remove-speechlist',
        //         '/send-all-secretary',
        //         '/send-editorial',
        //         '/send-to-editorial',
        //         '/send-to-secretary-single',
        //         '/share-delegates',
        //         '/stop-speech',
        //         '/stop-suggestions',
        //         '/stop-topic',
        //         '/suggestions-all',
        //         '/suggestions-details',
        //         '/teast',
        //         '/test',
        //         '/user',
        //         '/welcome'

        //         ]
        //     }
        // ]
    }
};