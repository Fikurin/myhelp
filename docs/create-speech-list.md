---
title: Speaker/Speechlist - Add to speechlist
---

## **How do i create speech list ?**

- Go to MyMeet Login to your MyMeet account

- On the sidebar click **Now** menu

 <img src="/images/now - create speech list/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

<img src="/images/now - create speech list/media/image2.png" width="601" height="150" />

- click on the topic you need to participate

- to create speech list you need to find button add speech list on the topic you have already selected <img src="/images/now - create speech list/media/image3.png" alt="Sebuah gambar berisi cuplikan layar, monitor Deskripsi dihasilkan secara otomatis" width="596" height="292" />

- After you click on the button new form will appear.

<img src="/images/now - create speech list/media/image4.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="601" height="246" />

- You can add your self as admin to the speech list by clicking add me to the speechlist for this topic

- Or you can set yourself as first speaker by clicking add me to the beginning of speechlist

- You can also find delegate who want to set as speechlist

- Or you can find guest

- You can find delegate by finding by id or name of delegate. Or by clicking on the list<img src="/images/now - create speech list/media/image5.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="601" height="400" />

- You can find guest by name or ide guest or by clicking the guest on the menu

<img src="/images/now - create speech list/media/image6.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="601" height="167" />
