---
title: Suggestions - My suggestions
---

## **How do I see all of** **my suggestions only?**

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click Suggestions menu

> <img src="/images/my suggestions/media/image1.png" width="197" height="113" />

- My suggestions will appear in list order by date created. <img src="/images/my suggestions/media/image2.png" width="601" height="193" />
- You can see every suggestion details after you click <img src="/images/my suggestions/media/image3.png" width="22" height="35" /> and Details button will appear

  <img src="/images/my suggestions/media/image4.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="180" height="89" />

- Click Details and you will see the suggestions details

  <img src="/images/my suggestions/media/image5.png" width="484" height="236" />

- Click back to go <img src="/images/my suggestions/media/image6.png" width="81" height="35" />to the previous page
