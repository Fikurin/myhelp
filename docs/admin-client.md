---
title: Manage Clients
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## **How do I create client account?**

- Go to MyMeet. Login to your MyMeet account as an admin

- Go to client menu on the sidebar

 <img src="/images/admin-clients/media/image1.png" width="403" height="357" />

- Click on the “plus” icon on the corner left-bottom

<img src="/images/admin-clients/media/image2.png"  width="323" height="147" />

- A form will pop out for you to fill client information that can be use to login into MyMeet

<img src="/images/admin-clients/media/image3.png"  width="600" height="511" />

- As you can see there’s several form to fill,  such as :
  - **First name** : please fill with your client first name.
  - **Last name** : please fill with your client last name.
  - **Email** : please fill with your client email.
  - **Password** : please fill with your client password, you can give default password for your clients. They can change it later.
  - **Enter Password confirmation** : please fill with your client password from password form on the left side.
  - **Description** : please fill with your client description, it will not appear to your client.
- Click **Save** to store your provided information and client can login directly to client account using user credential you have inserted.
- Or **Cancel** to stop add client process.

## **How can i edit client information ?**

- Go to MyMeet. Login to your MyMeet account as an admin

- Go to client menu on the sidebar

<img src="/images/admin-clients/media/image4.png" width="403" height="357" />

- As you can see list of user and go straight to the icon looks like this <img src="/images/admin-clients/media/image5.png" width="22" height="35" /> on the right side of every row of user account

- and you will have 3 more option menu that you can click

 <img src="/images/admin-clients/media/image6.png" width="202" height="181" />

- **View Details** : you can look into details of the client
- **Edit : you** can edit the details of the client
- **Delete** : you can delete client at this row forever

* And then click Edit so you can change client details and form will pop out looks like this

 <img src="/images/admin-clients/media/image7.png" width="600" height="510" />

- As you can see there’s several form to fill, such as :

  - **First name** : please fill with your client first name.
  - **Last name** : please fill with your client last name.
  - **Email** : please fill with your client email.
  - **Password** : leave out empty if you didn’t want to change client password
  - **Enter Password confirmation** : same as password form you can leave this out empty if you didn’t want to change client password
  - **Description** : please fill with your client description, it will not appear to your client.
  - **Password** and **Enter password** will always blanked, fill empty if you didn’t want to change client password.

- Click **Save** to store your provided information and client can login directly to client account using user credential you have inserted.

- Or **Cancel** to stop add client process.

## **How can I delete a client ?**

- Go to MyMeet. Login to your MyMeet account as an admin

- Go to client menu on the sidebar

<img src="/images/admin-clients/media/image4.png" width="403" height="357" />

- As you can see list of user and go straight to the icon looks like this <img src="/images/admin-clients/media/image5.png" width="22" height="35" /> on the right side of every row of user account

- and you will have 3 more option menu that you can click

 <img src="/images/admin-clients/media/image6.png" width="202" height="181" />

- **View** Details : you can look into details of the client
- **Edit** : you can edit the details of the client
- **Delete** : you can delete client at this row forever
- Click Delete so you can see prompt are you sure to Delete this client ?

 <img src="/images/admin-clients/media/image8.png" width="599" height="170" />

- You can press **Delete** to remove account or **Cancel** to stop delete process
