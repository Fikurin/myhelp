---
title: Suggestions - Send Suggestions to Editorial A/B/C
---


::: tip Warning!!
This page can be accessed only by admin.
:::


## **How do I send Suggestions to editorial?**

<span id="_Hlk530904872" class="anchor"></span>

- Go to **MyMeet** Login to your MyMeet account
- On the sidebar click **Suggestions** menu and click on the process sugestions

> <img src="/images/secretary - send to editorial/media/image1.png" width="187" height="328" />

- You will see your form process suggestions for secretary

  <img src="/images/secretary - send to editorial/media/image2.png" width="601" height="242" />

- You can process suggestions by clicking on the suggestions list on your left numbering and named by items and status
- You can click **send to** editorial so editorial can review and process suggestions
