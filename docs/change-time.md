---
title: Speaker/Speech List - Change Speaker Timer
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## How do I change time to speech?

- Go to **MyMeet** Login to your MyMeet account
- On the sidebar click **Now** menu

 <img src="/images/now -  change time/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now -  change time/media/image2.png" width="601" height="150" />

- click on the topic you need to participate

  <img src="/images/now -  change time/media/image3.png" alt="Sebuah gambar berisi cuplikan layar, monitor Deskripsi dihasilkan secara otomatis" width="400" height="" />

- on the right corner of your screen you can see speech list form you can click on the three dot <img src="/images/now -  change time/media/image4.png" width="22" height="35" /> on the row of every speechlist and click edit time to change time of the speech list. Then new form will appear

  <img src="/images/now -  change time/media/image5.png" width="400" height="" />

- click save to store the new time to the database.
