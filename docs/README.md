---
home: true
actionText: Explore Guide
actionLink: /login-logout
features:
  - title: General
    details: Learn how to edit and change langguage here
    actionLink: /login-logout
  - title: Manage Clients
    details: Learn how to manage clients, add, edit, remove and many more
    actionLink: /admin-client
  - title: Manage Users
    details: Learn how to manage your users in simple steps
    actionLink: /user
  - title: Manage Events
    details: Learn how to create and manage events
    actionLink: /client-event
  - title: Event Feature
    details: Learn how all feature in event MyMeet works
    actionLink: /agenda
  - title: Event Now
    details: Learn how to create and run eventon MyMeet
    actionLink: /change-order
---
