---
title: Topic - Activate the Topic
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## How do I activate the topic ?

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click **Now** menu

 <img src="/images/now - activate the topic/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now - activate the topic/media/image2.png" width="601" height="150" />

- click on the topic you need to participate
- you can find start this topic button on the top left of the suggestions

  <img src="/images/now - activate the topic/media/image3.png" width="100" height="" />

And topic you pick will started
