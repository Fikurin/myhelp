---
title: Speaker/Speech List - Remove Speaker
---


::: tip Warning!!
This page can be accessed only by admin.
:::


## **How do I remove speechlist?**

- Go to MyMeet Login to your MyMeet account
- On the sidebar click **Now** menu

> <img src="/images/now -remove speechlist/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now -remove speechlist/media/image2.png" width="601" height="150" />

- click on the topic you need to participate

  <img src="/images/now -remove speechlist/media/image3.png" alt="Sebuah gambar berisi cuplikan layar, monitor Deskripsi dihasilkan secara otomatis" width="601" height="315" />

- on the right corner of your screen you can see speech list form you can click on the three dot <img src="/images/now -remove speechlist/media/image4.png" width="22" height="35" /> on the row of every speechlist and click delete to remove speech list

  <img src="/images/now -remove speechlist/media/image5.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="600" height="254" />

- click delete to completely remove the speaker on the speechlist
