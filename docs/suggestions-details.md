---
title: Suggestions - Suggestions Details
---
## **How do i view suggestion details ?**

 <span id="_Hlk530904872" class="anchor"><span id="_Hlk530905758" class="anchor"></span></span>

* Go to **MyMeet** Login to your MyMeet account
* On the sidebar click **Now** menu

> <img src="/images/now - suggestions details/media/image1.png" width="137" height="309" />

* then go to the active topic and click the topic

  <img src="/images/now - suggestions details/media/image2.png" width="601" height="150" />

* click on the topic you need to participate
* to remove your suggestion click on the three dots <img src="/images/now - suggestions details/media/image3.png" width="22" height="35" /> and then new button will appear 
  <img src="/images/now - suggestions details/media/image4.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="576" height="113" />
* click view suggestion to show the suggestions details
