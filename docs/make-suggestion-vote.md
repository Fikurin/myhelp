---
title: Make Suggestion Vote
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## How can I make a **suggestion vote**?

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click **Now** menu

> <img src="/images/image1.png" alt="https://myhelp.ajak.in/images/now%20%20-%20send%20to%20secretary%20single/media/image2.png" style="width:1.25903in;height:" />

- then go to the active topic and click the topic

> <img src="/images/image2.png" alt="https://myhelp.ajak.in/images/now%20%20-%20send%20to%20secretary%20single/media/image2.png" style="width:6.25903in;height:1.56458in" />

- click on the topic you need to Elctronic vote or Quick Vote, and popup will appear:

> <img src="/images/image3.png" alt="https://myhelp.ajak.in/images/now%20%20-%20send%20to%20secretary%20single/media/image2.png" style="width:3.25903in;height:" />

## How can I make **Quick Vote**?

- Click **Start vote** Button, there are two choices, **Quick vote** and **Electronic vote**

> <img src="/images/image4.png" alt="https://myhelp.ajak.in/images/now%20%20-%20send%20to%20secretary%20single/media/image2.png" style="width:1.25903in;height:" />

- Choose **Electronic Vote** button for a electronic vote in selected suggestion, and a popup will appear as below:

> <img src="/images/image3.png" alt="https://myhelp.ajak.in/images/now%20%20-%20send%20to%20secretary%20single/media/image2.png" style="width:3.25903in;height:" />

- Choose **Yes** button if you agree with the suggestion, and **No** button for disagree with a suggestion or **Blank** for neutral

Click **Stop Voting** button if voting has been done, **Display Voting** button is useful to display ongoing voting

## How can I make **Quick Vote**?

- Click **Start vote** Button, there are two choices, **Quick vote** and **Electronic
  **
- Choose **Quick Vote** button for a quick vote in selected suggestion, and a popup will appear as below:

> <img src="/images/image6.png" alt="https://myhelp.ajak.in/images/now%20%20-%20send%20to%20secretary%20single/media/image2.png" style="width:4.25903in;height:" />

- Choose **Yes** button if done, and **No** button for cancelling **Quick Vote**.
