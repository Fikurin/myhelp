---
title: "Export Meeting "
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## **How do I export meeting data ?**

1. Go to **MyMeet. **Login to your MyMeet account
2. On the sidebar click Event menu

<img src="/images/export meeting/media/image1.png" width="317" height="241" />

3. Click on the event you want to export. And on the right side of corner in the menu you will see export meeting button
4. Click on the export meeting and export menu will appear

 <img src="/images/export meeting/media/image3.png" alt="Sebuah gambar berisi elektronik Deskripsi dihasilkan secara otomatis" width="601" height="175" />

- **Topics** : to download topics/agenda in xls file
- **participants** : to download all participant in xls file
- **editorial team** : to download editorial team in xls file
- **speechlist** : to download speechlist in xls file
- **suggestions** : to download suggestions in xls file
- **votings** : to download votings in xls file
- **Custom** votings : to download custom votings in xls files
- **Messages** : to download all messages in xls files
