---
title: Topic - Edit Topic Settings
---
::: tip Warning!!
This page can be accessed only by admin.
:::

## **How can I Edit Topic?**

<span id="_Hlk530904872" class="anchor"><span id="_Hlk530905758" class="anchor"></span></span>

- Go to **MyMeet** Login to your MyMeet account
- On the sidebar click **Now** menu

 <img src="/images/now  - edit topic/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now  - edit topic/media/image2.png" width="601" height="150" />

- click on the topic you need to participate
- you can find grind button on the right top on the form

  <img src="/images/now  - edit topic/media/image3.png" width="60" height="" />

- And new form will appear

  <img src="/images/now  - edit topic/media/image4.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="601" height="420" />
