---
title: Manage Editorial Teams
---

---

::: tip Warning!!
This page can be accessed only by admin.
:::

**Managing Events – Editorial Teams**

You can create a team for editorial, where you can give a name and description to the team as needed. You can make one team or many teams on the Editorial Team.

## **How can I add Editorial Teams of events in MyMeet?**

To see your **Editorial Teams** page of MyMeet:

- Click <img src="/images/event - Editorial/media/image1.png" width="106" height="" /> at the top left under **Dashboard** icon of any MyMeet Page. You’ll be on the **Managing Events** page of MyMeet.
- Select the **Event** for which you will add Agenda in **Event List**.

 <img src="/images/event - Editorial/media/image3.png" width="365" height="182" />

- Then click the <img src="/images/event - Editorial/media/image2.png" width="184" height="22" /> button. You’ll be on the **Editorial Teams** page of MyMeet.
- Then click the <img src="/images/event - Editorial/media/image4.png" width="29" height="" /> **red button** for **Add Editorial Teams**.
- As you can see there are several forms to fill out, as below:

* **Create Editorial Team** :
  - Name : The name of the Editorial team.
  - Description : Description of the Editorial team.
  - Click the **Submit** button if done, or **Cancel** button for canceling.

**How can I see the detail for a specific Editorial Teams of events in MyMeet?**

- Click <img src="/images/event - Editorial/media/image7.png" width="37" height="36" /> button on a list of the Editorial Team you want to see detail You will see 3 options there, click <img src="/images/event - Editorial/media/image6.png" width="114" height="29" /> **View Details**
- As you can see, there’s the detail of Editorial Team, click **Back** button to back to the Editorial Team list page

**How can I Edit for a specific Editorial Teams for events in MyMeet?**

- Click <img src="/images/event - Editorial/media/image7.png" width="37" height="36" /> button on a list of the Editorial Topic you want to edit, you will see 3 options there, click <img src="/images/event - Editorial/media/image10.png" width="164" height="36" /> **Edit **
- As you can see, You can **Edit Editorial Team,** starting from the name to the description. Click the **Save** button if done, or **Cancel** button for canceling.

**How can I Delete for a specific Editorial Teams for events in MyMeet?**

- Click <img src="/images/event - Editorial/media/image7.png" width="37" height="36" /> button on a list of the Editorial Topic you want to delete, you will see 3 options there, click <img src="/images/event - Editorial/media/image12.png" width="166" height="39" /> **Delete.**
- As you can see, You can **Delete Editorial Team,** click **Delete** to remove **Editorial Team** or **Cancel** to stop delete process.

**Search editorial team list with a search box.**

- To searching a specific editorial team data, you can use the **Search** box as below:

**Filtering editorial team list for a certain amount.**

- To show editorial team data for a specific range, you can fill the **Show** box with a specific number as below:
