---
title: Custom Vote
---

::: tip Warning!!
This page can be accessed only by admin.
:::

**How can I make Custom Vote?**
 
<span id="_Hlk530423468" class="anchor"></span>You can **make Custom Vote ** on the **Setting** page of MyMeet.

**How can I make a custom vote for  events in MyMeet?**

To see your **Settings** page of MyMeet:

- Click <img src="/images/Events - Settings/media/image1.png" width="106" height="" /> at the top left under **Dashboard** icon of any MyMeet Page. You’ll be on the **Managing Events** page of MyMeet.
- Select the **Event** for which you will add Agenda in **Event List**.

 <img src="/images/Events - Settings/media/image3.png" width="365" height="182" />

- Then click the <img src="/images/Events - Settings/media/image2.png" width="163" height="20" /> button. You’ll be on the **Settings** page of MyMeet.
- There are 4 tab sections in the **Settings**, the first is **Pause section**, choose **Custom Vote**, as below:
   As you can see there’s several tabs, such as:

   <img src="/images/Events - Settings/media/image4.png" width="300" height="" />
## **Custom Vote** :

  - You can create **Voting** in the **Setting** on the event you selected, just click for the <img src="/images/Events - Settings/media/image5.png" width="25" height="27" /> button. This setting will be useful when you want to make a custom vote at the moment.

  - Set the name on **Voting name.**

    <img src="/images/Events - Settings/media/image13.png" width="300" height="" />

  - Add option for voting with a click **Plus** <img src="/images/Events - Settings/media/image14.png" width="30" height="" /> button has a blue color and fill out the **Name,** click **Submit** button if done or **Cancel** button for cancelling the action.

    <img src="/images/Events - Settings/media/image15.png" width="300" height="" />

  - You can set the timer in minutes for a **Voting** on a specific event, as below:

    <img src="/images/Events - Settings/media/image16.png" width="200" height="" />

  - If done click **Add custom Voting** button, or **Reset** for cancelling the action.

    <img src="/images/Events - Settings/media/image17.png" width="400" height="" />

  - For start or stop the **Custom voting** you can click the **start** button <img src="/images/Events - Settings/media/image18.png" width="124" height="37" />
  - Or you can start/stop **Custom Voting** with a click the **three dots** button on a specific **Custom voting** data in the list, there 5 options, choose <img src="/images/Events - Settings/media/image19.png" width="97" height="" />Ofor start **Custom voting**, choose for stop **Custom voting.**
## How can I edit **Custom voting?** 
  - click the **three dots** button on a specific **Custom voting** data in the list, there 5 options, choose <img src="/images/Events - Settings/media/image20.png" width="97" height="" /> There popup will appear, fill out the **Voting Name**, **Voting Time**. then click **Update Voting** button if you want to update the data or Cancel **button** for cancelling the update.

    <img src="/images/Events - Settings/media/image21.png" width="346" height="157" />

## How can I see **Result Custom voting?** 
  
  - click the **three dots** button on a specific **Custom voting** data in the list, there 5 options, choose <img src="/images/Events - Settings/media/image22.png" width="97" height="20" />There popup will appear as below:

    <img src="/images/Events - Settings/media/image23.png" width="300" height="" />

  - If you start **Custom Voting,** when you go to **Now** page, the page will show the **Custom voting** form that you have made before.

    <img src="/images/Events - Settings/media/image24.png" width="300" height="" />

  - To stop **Custom Voting,** you can click **Stop** button.
  - If you stop **Custom Voting,** when you go to **Now** page, the page will be normal as it should be.
  - How can I see **Display** of **Custom Voting ?** click the **three dots** button on a specific **Custom voting** data in the list, there 5 options, choose <img src="/images/Events - Settings/media/image25.png" width="97" height="" />then you will be directed to the **Display** page.

- **Display General** :

## How can I create a **Display General?** 
  - Click “+” button. <img src="/images/Events - Settings/media/image26.png" width="200" height="" />
  - Popup will appear, fill out the Name, File form, then click **Submit** button if done or **Cancel** button for cancelling the action
  - You can see a preview display where the **Display** it’s displayed.
    <img src="/images/Events - Settings/media/image27.png" width="380" height="" />
  - You can set up the formatted **Display** for general, start from font to alignment.
    <img src="/images/Events - Settings/media/image28.png" width="380" height="" />
  - Click **Save** button if done or **Cancel** button for cancelling the action. <img src="/images/Events - Settings/media/image29.png" width="150" height="" />
