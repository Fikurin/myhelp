---
title: Suggestions - Share Suggestions to Delegates
---

::: tip Warning!!
This page can be accessed only by admin.
:::


## **How do I share suggestion to Delegate?**

- <span id="_Hlk530904872" class="anchor"></span>Go to **MyMeet. **Login to your MyMeet account
- On the sidebar click **Now** menu

> <img src="/images/now - share to delegates/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now - share to delegates/media/image2.png" width="601" height="150" />

- click on the topic you need to participate
- after you create your suggestions you can see your suggestions as list of suggestions
- click on the suggestions you want to share to delegates
- new form will appear

  <img src="/images/now - share to delegates/media/image3.png" width="359" height="375" />

- click share to delegates to share this topics
