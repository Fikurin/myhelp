---
title: "Reports "
---

::: tip Warning!!
This page can be accessed only by admin.
:::

**Managing Events – Reporting**

<span id="_Hlk530423468" class="anchor"></span>You can make a **Reporting** for each section that you follow in the MyMeet event, the report will automatically be generated as a text / .docx file. In the reporting page it can also convert sound files into text.

## **How can I create Reporting for each section of events in MyMeet?**

To see your **Reporting** page of MyMeet:

1. Click <img src="/images/Events - Reporting/media/image1.png" width="106" height="" /> at the top left under **Dashboard** icon of any MyMeet Page. You’ll be on the **Managing Events** page of MyMeet.
2. Select the **Event** for which you will add Agenda in **Event List**.

> <img src="/images/Events - Reporting/media/image3.png" width="365" height="182" />

3. Then click the <img src="/images/Events - Reporting/media/image2.png" width="206" height="20" /> button. You’ll be on the **Reporting** page of MyMeet.
4. Assuming there are 3 sections in the topic, the first is section A, as below:
   As you can see there’s several form to fill, such as:

- **Type** : You can choose the type for reporting in each section, there are five options that you can choose
- **Topics** : select the topic you want to enter in reporting.
- **Excerpt**: fill in with excerpt that you want to enter in reporting.

5. Assuming there are 3 sections in the topic, the second is section B, below:
   As you can see there’s several form to fill, such as:

- **Type** : You can choose the type for reporting in each section, there are five options that you can choose
- **Topics** : select the topic you want to enter in reporting.
- **Excerpt**: fill in with excerpt that you want to enter in reporting.

6. Assuming there are 3 sections in the topic, the third is section C, below:
   As you can see there’s several form to fill, such as:

- **Type** : You can choose the type for reporting in each section, there are five options that you can choose
- **Topics** : select the topic you want to enter in reporting.
- **Excerpt**: fill in with excerpt that you want to enter in reporting.

7. You can upload files in the form of audio with .flac extension to be transcribed with google cloud service, audio should not exceed one minute. Click **Choose File** <img src="/images/Events - Reporting/media/image10.png" width="79" height="26" /> button to find the file in your computer directory. Then click **Cancel** to stop process or **Submit** if done.
8. To **Transcribe** your audio file to text click Transcribe button, the button as below:
9. The last part is that you can also enter the voting result, the custom voting result and the sickleave list to be included in the reporting, check the box if you want to include them. Then click **Save Document** <img src="/images/Events - Reporting/media/image13.png" width="65" height="" /> to download the **Reporting** file.
