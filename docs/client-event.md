---
title: "Managing Events "
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## **How do I create Event?**

- Go to **MyMeet**. Login to your MyMeet account as an admin or as client
- Go to **EVENTS** menu on the sidebar
- Click on the “plus” icon on the corner left-bottom
- A form will pop out for you to fill event information that can be use to login into MyMeet

 <img src="/images/admin - client event/media/image3.png" width="517" height="292" />

- As you can see there’s several form to fill, such as :
  - **Name** : please fill with your event name.
  - **Union** : please fill with your Organisazation name/company name.
  - **Location** : please fill with your detail location event.
  - **Branch** : please fill with division from your organization/company.
  - **Notes** : please fill with your desired notes about this event.
  - **Google Map Link** : please fill with your link directly to the google maps location.
  - **Start Date** : please fill with when your event is starting
  - **End Date** : please fill with when your event is end
  - **Code** : please fill with number that can identified your event
- Click **Save** to store your provided information and create an event.
- Or **Cancel** to stop add event process.

## **How can I edit event information ?**

- Go to **MyMeet. **Login to your MyMeet account as an admin
- Go to event menu on the sidebar
- As you can see list of event in the left section, and then click the event you want to **Edit** <img src="/images/admin - client event/media/image4.png" width="601" height="148" />
- As you click the event you want to **Edit** then you will directed to event detail first and find the edit button on the right corner

 <img src="/images/admin - client event/media/image5.png" width="601" height="311" />

- And then click **Edit** so you can change Event details and form will pop out looks like this

 <img src="/images/admin - client event/media/image6.png" width="519" height="306" />

- As you can see there’s several form to fill, such as :
  - **Name** : please fill with your event name.
  - **Union** : please fill with your Organisazation name/company name.
  - **Location** : please fill with your detail location event.
  - **Branch** : please fill with division from your organization/company.
  - **Notes** : please fill with your desired notes about this event.
  - **Google Map Link** : please fill with your link directly to the google maps location.
  - **Start Date** : please fill with when your event is starting
  - **End Date** : please fill with when your event is end
  - **Code** : please fill with number that can identified your event
- Click **Save** to store your edited information of an event.
- Or **Cancel** to stop add event process.

## **How can I delete a event ? **

- Go to **MyMeet.** Login to your MyMeet account as an admin
- Go to **EVENTS** menu on the sidebar
- As you click the event you want to **Delete** then you will directed to event detail first and find the Delete button on the right corner

 <img src="/images/admin - client event/media/image7.png" width="603" height="295" />

- Click Delete so you can see prompt are you sure to **Delete** this event ?

 <img src="/images/admin - client event/media/image8.png" width="599" height="168" />

- You can press **Delete** to remove **Event** or **Cancel** to stop delete process

## How do i activate an event ?

- Go to **MyMeet.** Login to your MyMeet account as an admin
- Go to **EVENTS** menu on the sidebar
- Once you see all event, then select event you want to activate

 <img src="/images/admin - client event/media/image7.png" width="601" height="294" />

- As you can see on the right corner of the menu and button labeled **activate event** <img src="/images/admin - client event/media/image9.png" alt="Sebuah gambar berisi cuplikan layar, monitor, elektronik, etalase Deskripsi dihasilkan secara otomatis" width="254" height="45" />
- And you can deactive event by click button **Set As Complete**
- Or you can set event as an upcoming event so participant will notified first for next event <img src="/images/admin - client event/media/image11.png" alt="Sebuah gambar berisi cuplikan layar, orang Deskripsi dihasilkan secara otomatis" width="256" height="45" />
