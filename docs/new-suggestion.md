---
title: Suggestions - Create new suggestion
---

## **How do i create new suggestions? **

 <span id="_Hlk530490349" class="anchor"></span>

- Go to MyMeet. Login to your MyMeet account

-  On the sidebar click **Now** menu

<img src="/images/now - create new suggestion/media/image1.png" width="137" height="309" />

-  then go to the active topic and click the topic

<img src="/images/now - create new suggestion/media/image2.png" width="601" height="150" />

-  click on the topic you need to participate

<img src="/images/now - create new suggestion/media/image3.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="601" height="385" />

-  then click on the add suggestion button <img src="/images/now - create new suggestion/media/image4.png" alt="Sebuah gambar berisi cuplikan layar, orang Deskripsi dihasilkan secara otomatis" width="86" height="22" /> and new form will appear as you can see

<img src="/images/now - create new suggestion/media/image5.png" alt="Sebuah gambar berisi cuplikan layar, monitor Deskripsi dihasilkan secara otomatis" width="601" height="370" />

You can fill form with desired suggestion you want

- **Title** : you can fill with your desired title of suggestions
- **Line Number in Material** : form mean which line started to which line you want to referencing to suggest
- **Suggestions** : your own suggestions, remember you sould make context and easy to understand suggestions for editorial or secretary depending on wich event you participate or event policy
- **Suggestions type** : you sould pick one of the type. There add word, modify word and delete word.
- **Add me to speech list** : this mean after you suggest you want to directly talks in front of all of the audience.
- **Search** : youcan search material or you can make a full screen to read all the material by clicking the box button 
<img src="/images/now - create new suggestion/media/image6.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="40" height="40" />
