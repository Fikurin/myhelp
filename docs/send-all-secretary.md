---
title: Suggestions - Send Suggestions to Secretary
---


::: tip Warning!!
This page can be accessed only by admin.
:::


## **How do I Send suggestions to Secretary ?**

<span id="_Hlk530904872" class="anchor"><span id="_Hlk530905758" class="anchor"></span></span>

- Go to **MyMeet **Login to your MyMeet account
- On the sidebar click **Now** menu

 <img src="/images/now - send all  to secretary/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now - send all  to secretary/media/image2.png" width="601" height="150" />

- click on the topic you need to participate
- you can find process button on the top left of the suggestions

  <img src="/images/now - send all  to secretary/media/image3.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="223" height="186" />

- click on the send all to secretary to send all suggestions
