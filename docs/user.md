---
title: Manage Users
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## **How do I create user account?**

1. Go to **MyMeet.** Login to your MyMeet account as an admin

- Go to **USERS** menu on the sidebar

   <img src="/images/admin - user/media/image1.png" width="302" height="257" />

- Click on the “plus” icon on the corner left-bottom

   <img src="/images/admin - user/media/image2.png" width="323" height="147" />

- A form will pop out for you to fill user information that can be use to login into MyMeet

> <img src="/images/admin - user/media/image3.png" width="517" height="511" />

- As you can see there’s several form to fill, such as :
  - **First name** : please fill with your user first name.
  - **Last name** : please fill with your user last name.
  - **Email** : please fill with your user email.
  - **Password** : please fill with your user password, you can give default password for your user. They can change it later.
  - **Enter Password confirmation** : please fill with your user password from password form on the left side.
  - **Description** : please fill with your user description, it will not appear to your user.
  - **Role** : is what role you want to assign to your user on the meeting
- Click **Save** to store your provided information and user can login directly to user account using user credential you have inserted.
- Or **Cancel** to stop add user process.

## How can I edit user information ?

- Go to MyMeet. Login to your MyMeet account as an admin
- Go to user menu on the sidebar

  <img src="/images/admin - user/media/image1.png" width="402" height="357" />

- As you can see list of user and go straight to the icon looks like this <img src="/images/admin - user/media/image4.png" width="22" height="35" /> on the right side of every row of user account
- and you will have 3 more option menu that you can click

> <img src="/images/admin - user/media/image5.png" width="202" height="181" />

- **View Details** : you can look into details of the user
- **Edit** : you can edit the details of the user
- **Delete** : you can delete user at this row forever

<!-- -->

- And then click Edit so you can change user details and form will pop out looks like this

> <img src="/images/admin - user/media/image6.png" width="519" height="510" />

- As you can see there’s several form to fill, such as :
  - **First name** : please fill with your user first name.
  - **Last name** : please fill with your user last name.
  - **Email** : please fill with your user email.
  - **Password** : leave out empty if you didn’t want to change user password
  - **Enter Password confirmation** : same as password form you can leave this out empty if you didn’t want to change user password
  - **Description** : please fill with your user description, it will not appear to your user.
  - **Role** : is what role you want to assign to your user on the meeting
  - **Password** and **Enter password** will always blanked, fill empty if you didn’t want to change user password.
- Click **Save** to store your provided information and user can login directly to user account using user credential you have inserted.
- Or **Cancel** to stop add user process.

## How can I delete a user ?

- Go to MyMeet. Login to your MyMeet account as an admin
- Go to user menu on the sidebar

  <img src="/images/admin - user/media/image7.png" width="403" height="357" />

- As you can see list of user and go straight to the icon looks like this <img src="/images/admin - user/media/image4.png" width="22" height="35" /> on the right side of every row of user account
- and you will have 3 more option menu that you can click

> <img src="/images/admin - user/media/image5.png" width="202" height="181" />

- **View** Details : you can look into details of the user
- **Edit** : you can edit the details of the user
- **Delete** : you can delete user at this row forever

<!-- -->

- Click Delete so you can see prompt are you sure to Delete this user ?

> <img src="/images/admin - user/media/image8.png" width="599" height="169" />

- You can press **Delete** to remove account or **Cancel** to stop delete process
