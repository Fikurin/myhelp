---
title: Speaker/Speech List - Set Mark as Last Speaker
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## **how do i mark speechlist as last speeker?**

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click **Now** menu

## **how do i mark speechlist as last speeker**

- Go to **MyMeet.** Login to your MyMeet account
- On the sidebar click **Now** menu

 <img src="/images/now - mark as last speaker/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic
- click on the topic you need to participate
- on the right corner of your screen you can see speech list form you can click on the three dot <img src="/images/now - mark as last speaker/media/image4.png" width="22" height="35" /> on the row of every speechlist and click mark as last speaker to end the speechlist with selected as last speaker
- click mark as last speaker to proceed to next process and delete all speechlist except the one you pick as last speaker
