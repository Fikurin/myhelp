---
title: Auth
---

**Log Into Your MyMeet Account**

## **How do I log into MyMeet account?**

1. Go to MyMeet. At the center under **Log in**, enter one of the following:

- **Email: **You can log in with an email that's listed on your MyMeet account.
- **Password**, enter your password.
- **Remember me:** Check the box if you want to save your account in your device

2. Click **Log In**.

## **How do I log out of MyMeet?**

To **Log out** of MyMeet on a computer:

1. Click <img src="/images/General - Login Logout/media/image2.png" width="37" height="37" /> at the top left of any MyMeet Page.
2. Choose and click **Log out **
