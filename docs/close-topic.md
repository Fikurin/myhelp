---
title: Topic - Close Topic
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## How do I Close The Topic ?

Go to **MyMeet**. Login to your MyMeet account

- On the sidebar click **Now** menu

 <img src="/images/now - close topic/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now - close topic/media/image2.png" width="601" height="150" />

- click on the topic you need to participate
- you can find process button on the top left of the suggestions

  <img src="/images/now - close topic/media/image3.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="223" height="186" />

- click close this topic to end or close this topic as done
