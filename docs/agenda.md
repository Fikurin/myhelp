---
title: Topics/Agenda
---

::: tip Warning!!
This page can be accessed only by admin.
:::

## **Managing Events - Agenda**

<span id="_Hlk530423468" class="anchor"></span>In the agenda you can add what topics will be discussed at the event at that time, you can also set the speech list settings, and add what material will be added to the topic.Top of Form

**Various Information in Agenda.**

## 1. **Topics**

 <img src="/images/Event - Agenda/media/image1.png" width="624" height="" />

**You can see all the topics in events, as below:**

   <img src="/images/Event - Agenda/media/image2.png" width="321" height="" />

## 2. **Delegates**

   <img src="/images/Event - Agenda/media/image1.png" width="624" height="60" />

**You can see all delegates in events, as below:**

   <img src="/images/Event - Agenda/media/image3.png" width="330" height="" />

## 3. **Suggestion**

   <img src="/images/Event - Agenda/media/image1.png" width="624" height="60" />

**You can see all the suggestions in events, as below:**

   <img src="/images/Event - Agenda/media/image4.png" width="363" height="" />

## **How can I create Topic for events in MyMeet?**

To see your **Agenda** page of MyMeet account:

1. Click <img src="/images/Event - Agenda/media/image5.png" width="106" height="" /> at the top left under **Dashboard** icon of any MyMeet Page. You’ll be on the **Managing Events** page of MyMeet.
2. Select the **Event** for which you will add Agenda in **Event List**.

> <img src="/images/Event - Agenda/media/image7.png" width="365" height="182" />

1. Then click the <img src="/images/Event - Agenda/media/image6.png" width="218" height="28" /> button. You’ll be on the **Agenda** page of MyMeet.
2. There are two **Button** in **Agenda**, the first button is for **Add Topics** the button looks like this <img src="/images/Event - Agenda/media/image8.png" width="50" height="" /> , and the other button use for **Import ** **Participants, Topics,** and **Suggestions**.
3. Add Topic in the **Add Topics** pages, then click the **Plus** <img src="/images/Event - Agenda/media/image8.png" width="50" height="" /> button .
4. As you can see there are several forms to fill out and have two tabs at the top, on the first tab **Topic Details & Settings.**
   As you can see there are several forms to fill, such as:

- **Topics Details** :
  - Name : The name of the topic.
  - Description : Description of the topic.
  - Suggestion Code : This Code will
  - Suggestion Opened : Check the box if the events allow adding a suggestion.
  - Show Suggestions : Check the box to display the suggestion list table
  - Topic Code : Check the box to enable code for the topic
- **Speech List Settings** :
  The Number of times that delegates can speak.
  - Speech Times : How many times can delegates speak.
  - Reply Times : How many times can delegates reply.
  - Wrong Times : How many times can delegates wrong.
    The Maximum length of a delegate’s speech (in minutes):
  - First Times : How many lengths in minutes can the delegates speak on the first time.
  - Second Times : How many lengths in minutes can the delegates speak on the second time.
  - Third Times : How many lengths in minutes can the delegates speak on the third time.
  - Reply Settings : How many lengths in minutes can the delegates speak on reply.
  - Reply Timer : How many lengths in minutes can the delegates speak on reply.
  - Wrong Timer : How many lengths in minutes can the delegates speak on wrong.

> How can I Allows **Speech Opened**?

- Speech Opened : There's is 3 check box on **Speech Opened, **
  Check Box on Speech Opened to **Allow Speak, Allow Reply,** and **Allow Wrong. **
- Show Speech list : Check the box on Display speech list table to show list table of speech.

1. Second tab **Topic Materials** used to add material to the topic to be created. Click the **Save** button if done, or **Cancel** button for canceling.
2. How can I Import data **Participant, Topics, or Suggestion** from my computer directory?
   - **Import Participant:**

- Click button, You will see 3 options there, click **Import Participants.**
- And then choose a file in your computer directory with Choose File button,
  Click **Submit** button if done, and **Discard** button for canceling import Participants.
  - **Import Topics:**
- Click button, You will see 3 options there, click **Import Topics.**
- And then choose a file in your computer directory with Choose File button,
  Click **Submit** button if done, and **Discard** button for canceling import Topics.
  - **Import Suggestions:**
- Click button, You will see 3 options there, click **Import Suggestions.**
- And then choose a file in your computer directory with Choose File button,
  Click **Submit** button if done, and **Discard** button for canceling import Suggestions.

**How can I create Sub Topic for events in MyMeet?**

- Click <img src="/images/Event - Agenda/media/image18.png" width="37" height="36" /> button on a list of the topic you want to add sub-Topic, You will see 3 options there, click **Add New Topics ** <img src="/images/Event - Agenda/media/image19.png" width="141" height="" />
- As you can see there are several forms to fill out and have two tabs at the top, on the first tab **Topic Details & Settings.**
- As you can see there are several forms to fill, such as:

<!-- -->

- **Topics Details** :
  - Name : The name of the topic.
  - Description : Description of the topic.
  - Suggestion Code : This Code will
  - Suggestion Opened : Check the box if the events allow adding a suggestion.
  - Show Suggestions : Check the box to display the suggestion list table
  - Topic Code : Check the box to enable code for the topic
- **Speech List Settings** :
  The Number of times that delegates can speak.
  - Speech Times : How many times can delegates speak.
  - Reply Times : How many times can delegates reply.
  - Wrong Times : How many times can delegates wrong.
    The Maximum length of a delegate’s speech (in minutes):
  - First Times : How many lengths in minutes can the delegates speak on the first time.
  - Second Times : How many lengths in minutes can the delegates speak on the second time.
  - Third Times : How many lengths in minutes can the delegates speak on the third time.
  - Reply Settings : How many lengths in minutes can the delegates speak on reply.
  - Reply Timer : How many lengths in minutes can the delegates speak on reply.
  - Wrong Timer : How many lengths in minutes can the delegates speak on wrong.

## How can I Allows **Speech Opened**?

- Speech Opened : There's is 3 check box on **Speech Opened, **
  Check Box on Speech Opened to **Allow Speak, Allow Reply,** and **Allow Wrong. **
- Show Speech list : Check the box on Display speech list table to show list table of speech.
- Second tab **Topic Materials** used to add material to the topic to be created. Click the **Save** button if done, or **Cancel** button for canceling.

* The result will be as below:

 <img src="/images/Event - Agenda/media/image22.png" width="426" height="" />

**How can I Edit Headline Topic for events in MyMeet?**

- Click <img src="/images/Event - Agenda/media/image18.png" width="37" height="36" /> button on a list of topic you want to edit, You will see 3 options there, click <img src="/images/Event - Agenda/media/image23.png" width="132" height="29" /> **Edit Headlines **
- As you can see, You can **Edit Headline Topic,** starting from the name to the notification. You can also change import files that have been made on the topic before. Click the **Save** button if done, or **Cancel** button for canceling.

**How can I Delete Topic for events in MyMeet?**

- Click <img src="/images/Event - Agenda/media/image18.png" width="37" height="36" /> button on a list of the topic you want to remove, You will see options there, click <img src="/images/Event - Agenda/media/image26.png" width="127" height="28" /> **Delete Headlines **
- You can click **Delete** to removeTopic Agenda or **Cancel** to stop delete process.
