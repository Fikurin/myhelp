---
title: Forgot Password
---

## **If you’re forgot your password?**

1.  Click **Forgot your Password?** under **Log in** button.

    <img src="/images/General - forgot password/media/image1.png" width="418" height="" />

2.  The popup will appear. At the center under **Forgot Password**, enter one of the following:

- **Email:** You can reset your password with an email that's listed on your MyMeet account.

- Click **Submit.**
