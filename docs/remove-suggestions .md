---
title: Suggestions - Remove Suggestions
---


::: tip Warning!!
This page can be accessed only by admin.
:::


## **How do i remove suggestions ?**

<span id="_Hlk530904872" class="anchor"><span id="_Hlk530905554" class="anchor"></span></span>

Go to MyMeet Login to your MyMeet account

- On the sidebar click **Now** menu

> <img src="/images/now - remove suggestions/media/image1.png" width="137" height="309" />

- then go to the active topic and click the topic

  <img src="/images/now - remove suggestions/media/image2.png" width="601" height="150" />

- click on the topic you need to participate

- after you create your suggestions you can see your suggestions as list of suggestions

- to remove your suggestion click on the three dots <img src="/images/now - remove suggestions/media/image3.png" width="22" height="35" /> and then new button will appear
  <img src="/images/now - remove suggestions/media/image4.png" alt="Sebuah gambar berisi cuplikan layar Deskripsi dihasilkan secara otomatis" width="576" height="113" />

- click delete to delete your suggestions. No. confirmations will appear so be carefull, once you have done it you cannot undo this process.
