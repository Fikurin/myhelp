---
title: Profile
---

**Your Profile and Setting of MyMeet**

You can edit your profile like your first name, last name, email, or you can change your password. Your profile also includes your **Joined Events**, where you can see your own event name and date you've been joined in events.Top of Form

## **How to see profile information of MyMeet account?**

To see your **Profile** of MyMeet account:

1. Click <img src="/images/General - Profile/media/image1.png" width="37" height="37" /> at the top left of any MyMeet Page.
2. Choose and click **Profile**. You’ll be on the profile page of MyMeet.
3. You can see your profile information like **Details** on your **Profile** page.
4. You can see your profile information like **Language** on your **Profile** page.
5. And you can see your list **Joined Events** of MyMeet on your **Profile** page.

**How do I edit profile of MyMeet account?**

To edit your **Profile** of MyMeet account:

1. Click <img src="/images/General - Profile/media/image1.png" width="37" height="37" /> at the top left of any MyMeet Page.
2. Choose and click **Profile**. You’ll be on the profile page of MyMeet.
3. At the top or left corner under your profile photo click <img src="/images/General - Profile/media/image6.png" width="121" height="" />
    **Edit Profile**, you’ll be on your **Edit Profile** page.
4. On **Edit Profile** page, enter one of the following:

- **First Name:** enter your first name.
- **Last Name:** enter your last name.
- **Email:** enter your email.
- **Password:** enter your new password.
- **Password Confirmation:** confirmation of your new password.
- **Description:** enter your description. E.g. : Administrator.
- **Image:** Upload your image file from your computer, and the image will display in your profile photo.

MyMeet

- **Language:** Choose your language for your profile of MyMeet, English or Norwegian.

MyMeetMyMeet

- **Send email form MyMeet to you:** Check the box if you want to MyMeet send email to you.
  MyMeet

## **How can I Register to the event of MyMeet?**

1. To check-in your **Account** to a venue the event of MyMeet, you can use your barcode, the **Barcode** will be shown if you click <img src="/images/General - Profile/media/image9.png" width="" height="33" /> button, in your **Joined Event,** as below:
2. As you can see, the **Barcode** you have will be different at each event. This **Barcode** is very usefull when you want to checkin at venue where the event will be held.
