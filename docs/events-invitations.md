---
title: Invitations
---
::: tip Warning!!
This page can be accessed only by admin.
:::

<span id="_Hlk530423468" class="anchor"></span>You can send **Notifications** to all participants about the event that will be held, also can send **Invitation** to all registered participants or some specific participants.

## **How can I send Invitation to a participant of events in MyMeet?**

To see your **Invitation** page of MyMeet:

- Click <img src="/images/Events - Invitations/media/image1.png" width="106" height="" /> at the top left under **Dashboard** icon of any MyMeet Page. You’ll be on the **Managing Events** page of MyMeet.
- Select the **Event** for which you will add Agenda in **Event List**.

 <img src="/images/Events - Invitations/media/image3.png" width="365" height="182" />

- Then click the <img src="/images/Events - Invitations/media/image2.png" width="210" height="25" /> button. You’ll be on the **Invitation** page of MyMeet.
- List of the participant.
   You can see the details for each participant in events, just click the participant you want to see, and the result as below:
- In the **Action** section, you can click **Send Invitation** for the participant you choose.

## **How can I send Notification to All participant of events in MyMeet?**

* Click the **Send Notification to all Participant** button, as below:

## **How can I send Invitation to All participant of events in MyMeet?**

* Click the **Send Invitation to all Participant** button, as below:

**Search Participant list with a search box.**

* To searching a specific participant data, you can use the Search box as below:
